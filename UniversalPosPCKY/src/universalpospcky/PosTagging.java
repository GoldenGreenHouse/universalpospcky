/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universalpospcky;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anna
 */
public class PosTagging {

    /**
     * @param args the command line arguments
     */
    
    static List<String> wordsDictionary;
    static List<String> tagsDictionary;
    static HashMap mTags;
    static HashMap mTagsTags;
    static HashMap mWordsTags;
    

    public static ArrayList<ArrayList<String[]>> scanFileTraining(String file)
    {
        File f = new File(file);
        Scanner s;
        
        ArrayList<ArrayList<String[]>> doc = new ArrayList<>();
        
        try {
            s = new Scanner(f);
              
            ArrayList<String[]> frase = new ArrayList<>();
           
            // Read each line, ensuring correct format.
            while (s.hasNext())
            {
                String index = s.next();         // indice parola nella frase  
                if(index.equals("1")){
                    //System.out.println("prima parole della frase - creo nuova lista");
                    if(!frase.isEmpty()){
                        doc.add(new ArrayList<>(frase));                        
                        frase.clear();  //al primo elemento della frase ripulisco la lista
                    }
                }  
                String[] parola = new String[2];
                parola[0] = s.next().toLowerCase(); //lettura della parola o del simbolo nella frase
                
                
                
                if(!wordsDictionary.contains(parola[0]))  //popola dizionario
                    wordsDictionary.add(parola[0]);
                
                s.next();         // lettura separatore _
                parola[1] = s.next();  //posTag 1
                if(!tagsDictionary.contains(parola[1]))
                    tagsDictionary.add(parola[1]);
                frase.add(parola);
                String posTag2 = s.next();  //posTag 2
                s.next();  //separatore
                s.next();  //numero
                s.next();  //tag
                s.next();  //sep
                s.next();  //sep               
            }
            //ultima frase da aggiungere in lista
            doc.add(frase);
            return doc;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PosTagging.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
      
    public static ArrayList<ArrayList<String>> scanFileTest(String file)
    {
        File f = new File(file);
        Scanner s;
        
        ArrayList<ArrayList<String>> doc = new ArrayList<>();//lista di frasi nel documento di test (solo parole, non servono tag!)
        
        try {
            s = new Scanner(f);
              
            ArrayList<String> frase = new ArrayList<>();
           
            // Read each line, ensuring correct format.
            while (s.hasNext())
            {
                String index = s.next();         // indice parola nella frase  
                if(index.equals("1")){
                    //System.out.println("prima parole della frase - creo nuova lista");
                    if(!frase.isEmpty()){
                        doc.add(new ArrayList<>(frase));                        
                        frase.clear();  //al primo elemento della frase ripulisco la lista
                    }
                }  
                String parola = s.next().toLowerCase(); //lettura della parola o del simbolo nella frase
                frase.add(parola); //aggiungo parola alla frase
                
                s.next();         // lettura separatore _
                s.next();  //posTag 1
                s.next();  //posTag 2
                s.next();  //separatore
                s.next();  //numero
                s.next();  //tag
                s.next();  //sep
                s.next();  //sep               
            }
            //ultima frase da aggiungere in lista
            doc.add(frase);
            return doc;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PosTagging.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
  
    
     //sviluppo array di frasi del testo su cui fare pos tagging con algoritmo di viterbi
    public static ArrayList<ArrayList<String>> scanFileObservation(String file){
        
        ArrayList<ArrayList<String>> observations = new ArrayList<>();        
        File f = new File(file);
        Scanner s;        
        
        try {
            s = new Scanner(f);
              
            ArrayList<String> frase = new ArrayList<>();
           
            // Read each line, ensuring correct format.
            while (s.hasNext())
            {
                String index = s.next();         // indice parola nella frase  
                if(index.equals("1")){
                    //System.out.println("prima parole della frase - creo nuova lista");
                    if(!frase.isEmpty()){
                        observations.add(new ArrayList<>(frase));                        
                        frase.clear();  //al primo elemento della frase ripulisco la lista
                    }
                }  
                String parola = s.next().toLowerCase(); //lettura della parola o del simbolo nella frase
                frase.add(parola); //aggiungo parola nella frase osservata
                s.next();         // lettura separatore _
                s.next();  //posTag 1
                String posTag2 = s.next();  //posTag 2
                s.next();  //separatore
                s.next();  //numero
                s.next();  //tag
                s.next();  //sep
                s.next();  //sep               
            }
            //ultima frase da aggiungere in lista
            observations.add(frase);
            return observations;
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PosTagging.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    //numero di volte in cui un tag compare nel documento (riscrivere ciclando sul dizionario dei tags)
    public static HashMap<String, Integer> contTags( ArrayList<ArrayList<String[]>> docList)
    {
        HashMap<String, Integer> map = new HashMap<>();
        
        int numFrasi = docList.size();  //numero di frasi nel documento di training --> è anche il numero di volte in cui c'è il tag "S" 
        
        
        for(ArrayList<String[]> l : docList){
            for(String[] s : l){
                if(map.containsKey(s[1])){
                    int cont = map.get(s[1]);
                    cont++;
                //    System.out.println(s[1] + " " + cont);
                    map.put(s[1], cont);
                }
                else{
                    map.put(s[1], 1);
                }
            }
        }
        
        map.put("S", numFrasi);  //il tag "tacito" di inizio frase compare tante volte quante le frasi nel documento!
        map.put("F", numFrasi); //tag tacito di fine frase
        return map;
    }
    
    //numero di volte in cui una certa sequenza di tag presente nel testo vi compare
    public static HashMap<String, Integer> contTagsTags(ArrayList<ArrayList<String[]>> docList)
    {
        HashMap<String, Integer> map = new HashMap<>();
        
        for(ArrayList<String[]> l : docList){  //per ogni frase
            Iterator<String[]> s = l.iterator();
            String prec = new String();
            if(s.hasNext())  //se ci sono parole nella frase, allora inizializzo il tag precedente
                prec = "S";
            
            String[] curr = new String[2]; 
            
            while(s.hasNext()){   //finchè ci sono parole nella frase
                curr = s.next();       //coppia (parola - tag) corrente
                int cont = 0; //contatore combinazione tags
                String key = prec+"*"+curr[1];  //chiave nella mappa delle combinazioni prec/succ dei tag
                
                if(map.containsKey(key)){
                    cont = map.get(key);
                    cont++;
                    map.put(key, cont);
                }
                else{
                    cont++;
                    map.put(key, cont);
                }
            //    System.out.println(key + "  " + cont);
                prec = curr[1];
            }
            
            if(!s.hasNext()){
                int cont = 0;
                String key = prec+"*"+"F";  //chiave nella mappa delle combinazioni prec/succ dei tag
                
                if(map.containsKey(key)){
                    cont = map.get(key);
                    cont++;
                    map.put(key, cont);
                }
                else{
                    cont++;
                    map.put(key, cont);
                }
            }
        }  
        return map;
    }    
       
    //conto numero di volte in cui il tag compare associato a quella parola
    public static HashMap<String, Integer> contWordsTags(ArrayList<ArrayList<String[]>> docList){
        HashMap<String, Integer> map = new HashMap<>();
        
        for(ArrayList<String[]> l : docList){
            for(String[] s : l){
                String key = s[0]+"*"+s[1]; //word*tag
//                System.out.println("Key: "+key);
                
                if(map.containsKey(key)){
                    int cont = map.get(key);
                    cont++;
                    map.put(key,cont);
                }
                else{
                    map.put(key, 1);
                }
            }
        }
        return map;
    }
    
    //calcolo probabilità che noto il tag prec, curr sia il successivo 
    //probPosPos(tagsDictionary.get(k), s)
    public static double probPosPos(String prec, String curr)
    {
        double prob = 0;  //se non c'è una sequenza di tag prec->curr nel training, la prob restituita deve essere 0
        String key = prec+"*"+curr;
        
        if(mTagsTags.containsKey(key)){
            Integer contPrecCurr = (Integer)mTagsTags.get(key);
            Integer contPrec = (Integer)mTags.get(prec);
            prob = contPrecCurr.doubleValue() / contPrec.doubleValue(); 
        }
        return prob;
    }
    
    //calcolo probabilità che noto il tag, la parola ad esso associata sia proprio word (se un tag non è mai associato a una parola, la prob è 0)
    public static double probPosWord(String tag, String word)
    {
        double prob = 0;
        String key = word+"*"+tag;
        if(mWordsTags.containsKey(key)){  //se alla parola è associato quel tag, calcolo la probabilità
            Integer contTagWord = (Integer)mWordsTags.get(key);
            Integer contTag = (Integer)mTags.get(tag);
            prob = contTagWord.doubleValue() / contTag.doubleValue(); 
        }
        return prob;  //se a word non è associato mai tag, mWordsTags.containsKey(key) è false! la prob è 0
    }
    
    public static String[][] Viterbi(ArrayList<String> frase){
        String[][] posTagger = new String[frase.size()][2]; //array con tag - probabilità
        
        double[][] viterbi = new double[tagsDictionary.size()+2][frase.size()];  //path probability matrix
        int[][] backpointer = new int[tagsDictionary.size()+2][frase.size()];  //index matrix
        
        for(int i = 0; i < tagsDictionary.size(); i++){      //initialization Step: aggiorno probabilità prima parola per tutti i tag che essa può assumere (NO S O F!!) 
            viterbi[i][0] = probPosPos("S", tagsDictionary.get(i)) * probPosWord(tagsDictionary.get(i), frase.get(0));
            backpointer[i][0] = 0;
        }
        
        for(int j = 1; j < frase.size(); j++){      //recursion step : per ogni parola della frase, dalla seconda in poi
            String parola = frase.get(j);
           
            for(int i = 0; i < tagsDictionary.size(); i++){    //calcolo prob del path 
                String s = tagsDictionary.get(i);
                double max = 0.0;
                double maxP = 0.0;
                int argMax = 0;
                
                for(int k = 0; k< tagsDictionary.size(); k++){  //ciclo calcolo max e argmax
                    String sp = tagsDictionary.get(k);
                    double pathProb = 0;
                    
                    if(wordsDictionary.contains(parola))
                        pathProb = viterbi[k][j-1] * probPosPos(sp, s) * probPosWord(s, parola);
                    else if(!wordsDictionary.contains(parola)){
                        //Rilassamento con equiprobabilità tra i pos successivi a quello visto
                        pathProb = viterbi[k][j-1] * probPosPos(sp, s) * (1.0/(double)tagsDictionary.size());
                    }
                    
                    if(pathProb > max) {
                        max = pathProb;  
                    } 
                    
                    if(viterbi[k][j-1] * probPosPos(sp, s) > maxP){
                        maxP = viterbi[k][j-1] * probPosPos(sp, s);
                        argMax = k;
                    } 
                }
                
                viterbi[i][j] = max;
                backpointer[i][j] = argMax;
                
            }
            
        }
        
        //final step!!!        
        // Consideriamo F come tag finale, analogamente come S viene considerato come iniziale
        double max1 = 0;
        int argMax1 = 0;
        for(int k = 0; k< tagsDictionary.size(); k++){
            if(viterbi[k][frase.size()-1] * probPosPos(tagsDictionary.get(k), "F") > max1){
                max1 = viterbi[k][frase.size()-1] * probPosPos(tagsDictionary.get(k), "F");
                argMax1 = k;
            }
        }
        
        viterbi[tagsDictionary.size()][frase.size()-1] = max1;
        backpointer[tagsDictionary.size()][frase.size()-1] = argMax1;
        
        //RIAVVOLGIMENTO, POS TAGGING VERO E PROPRIO
       
        int index = backpointer[tagsDictionary.size()][frase.size()-1];
        
        for(int i=frase.size()-1; i>=0; i--){
            
            posTagger[i][0] = tagsDictionary.get(index);
            posTagger[i][1] = Double.toString(viterbi[index][i]);
            
            index = backpointer[index][i];
            //System.out.println(viterbi[index][i]);
        }
        
        return posTagger;
        
    } //fine function viterbi
    
}