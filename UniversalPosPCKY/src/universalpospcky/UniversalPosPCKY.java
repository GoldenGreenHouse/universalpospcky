/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package universalpospcky;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Anna
 */
public class UniversalPosPCKY {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
   
        //**** POS TAGGING ****
        String trainingPath = "docs\\universal_treebanks_v2.0\\std\\it\\it-universal-train.conll";
        String testingPath = "docs\\universal_treebanks_v2.0\\std\\it\\it-universal-test.conll";
        
        PosTagging.wordsDictionary = new ArrayList<>();  //dizionario delle parole (observations, O)
        PosTagging.tagsDictionary = new ArrayList<>();   //dizionario dei tag  (states, S)
       
        ArrayList<ArrayList<String[]>> trainingFile = PosTagging.scanFileTraining(trainingPath);  //file come lista di frasi  
        ArrayList<ArrayList<String>> frasiTest = PosTagging.scanFileTest(testingPath);
        
        PosTagging.mTags = PosTagging.contTags(trainingFile);   //lista contenente il numero di volte in cui ciascun tag compare nel testo
        PosTagging.mTagsTags = PosTagging.contTagsTags(trainingFile);  //lista contenente il numero di volte in cui ciascuna sequenza (prec,curr) di tag nel testo compare
        PosTagging.mWordsTags = PosTagging.contWordsTags(trainingFile); //lista con il numero di volte in cui il tag compare asssociato ad una data parola
        float match=0;
        float numParole = 0;
        
        ArrayList<String[][]> tagsProb = new ArrayList<>();    //array in output da viterbi, con tag assegnati e probabilità con cui vengono assegnati
        
        //viterbi, pos tagging
        for (ArrayList<String> frase : frasiTest) {     //per ogni frase
            String[][] tag1 = PosTagging.Viterbi(frase);        //genero array posTag + probabilità
            tagsProb.add(tag1); //aggiungo alla lista i tag assegnati da viterbi per la frase passata
          /*  System.out.println(frase);
            for (int i = 0; i<frase.size(); i++)
                System.out.print(tag1[i][0] + "   " + tag1[i][1] + "   ");
            
            System.out.println("\n");  */
        }
        
        //***** UPCKY *****
        
        String path = "docs\\new-treebank-POS-GOOGLE.txt";
        String fileGramm = "docs\\grammaticaGoogle.txt";
        
        //Pattern è una classe di Java 
        try {
            //frasiTest = UPCKY.produciFrasiTest(path);
            UPCKY.readGrammar(fileGramm);
            UPCKY.initializeNTMaps();
            UPCKY.parseSentences(frasiTest, tagsProb);    //dare all'algoritmo sia le frasi da testare che il postag con relativa probabilità
            //da modificare: scrivere solo le ultime frasi (test) in apposito file, da cui poi partire per rilevare gli alberi con PCKY
        } catch (IOException ex) {
            Logger.getLogger(UPCKY.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(UPCKY.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("\n Match: "+UPCKY.contMatch);
        
    }
    
}
