/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package universalpospcky;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Arrays;

/**
 *
 * @author Alessandro
 */
public class UPCKY {

    /**
     * @param args the command line arguments
     */
    //hashmap for binary productions and their probabilities
    private static Map<String,Double> bGrammar = new HashMap<String,Double>();
    
    //hashmap for lexical productions and their probabilities
    private static Map<String,Double> lGrammar = new HashMap<String,Double>();
    
    //set of all the non-terminals in the grammar
    private static Set<String> nonTerminals = new HashSet<String>();
    
    //optimization hashmap from string of non-terminal to integer representation
    private static Map<String,Integer> ntToInt = new HashMap<String,Integer>();
    
    //optimization hashmap from integer representation of non-terminal to string representation
    private static Map<Integer,String> ntToString = new HashMap<Integer,String>();
    
    //list of the parses returned for the input sentences
    private static List<String> parses = new ArrayList<String>();
    
    public static int contMatch = 0;
    
    public static ArrayList<ArrayList<String>> produciFrasiTest(String path) throws IOException{
        
        Pattern p = Pattern.compile("\\s[a-z0-9\\w\\,\\.\\;\\:\\*\\'\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
        
        ArrayList<ArrayList<String>> frasiTest = new ArrayList<>();
        //da modificare: scrivere solo le ultime frasi (test) in apposito file, da cui poi partire per rilevare gli alberi con PCKY

        BufferedReader br = new BufferedReader(new FileReader(path));
        String line;
        int cont = 0;
        while ((line = br.readLine()) != null) {
            cont++;
            // process the line
            if(cont >= 985){
                ArrayList<String> frase = new ArrayList<>();
                //cerca in tutta la stringa le parole che matchano con l'espressione regolare in p
                Matcher m = p.matcher(line);
                while(m.find()){
                    String matched = m.group();
                   // System.out.print(" " + matched);
                    if(frase.isEmpty())
                        frase.add(matched);
                    else if(!frase.get(frase.size()-1).equals(matched))
                        frase.add(matched);
                }
                frasiTest.add(frase);
                //System.out.println("\n");
            }
        }
        
        return frasiTest;
    }
    
     /**
     * readGrammar reads a probabilistic grammar file and stores each production with its
     * probability in a hashmap for the binary productions and a hashmap for the lexical productions.
     * As it parses the file it also builds the set of non-terminals
     * @param filename String for file path to the probabilistic grammar file
    **/
    public static void readGrammar(String filename) throws FileNotFoundException,IOException, Exception {
        System.out.println("readGrammar  "+ filename);
        
	BufferedReader in = new BufferedReader(new FileReader(filename));
        
        Pattern p1 = Pattern.compile("[\\w\\.\\-]+"); //pattern probabilità
        Pattern lex = Pattern.compile("[a-z0-9\\w\\,\\.\\;\\:\\*\\\"\\-\\_\\à\\è\\ì\\ò\\ù\\À\\+"
            + "È\\Ì\\Ò\\Ù\\á\\é\\í\\ó\\ú\\ý\\Á\\É\\Í\\Ó\\Ú\\Ý\\â\\ê\\î\\ô\\û\\Â\\Ê\\Î\\Ô\\Û\\ã\\ñ\\"
            + "õ\\Ã\\Ñ\\Õ\\ä\\ë\\ï\\ö\\ü\\ÿ\\Ä\\Ë\\Ï\\Ö\\Ü\\Ÿ\\ç\\Ç\\ß\\Ø\\ø\\Å\\å\\Æ\\æ\\œ]+");
       
        String next = in.readLine(); //leggo la prima riga del file grammatica
        
        String[] items;
        String[] destra; //per la parte sinistra della produzione della grammatica, in cui vado a individuare la probabilità
        String production;
        Double prob;
        
        //parse grammar file
        while(next != null){
            items = next.split(" -> ");		//splitto la riga distinguendo parte destra e parte sinistra          
            
            //System.out.println(items[0] + "        " + items[1]);
            //items[0] sarà parte destra, items[1] la sinistra ma ci sono regole senza parte sinistra!!
            
            //lavoro sulla parte sinistra: splitto per trovare la probabilità, nell'ultima locazione
            destra = items[items.length-1].split("\\s");
            
            String probS = destra[destra.length-1];
            
            Matcher m = p1.matcher(probS);    //estraggo probabilità vera e proria, eliminando le parentesi
            m.find();
            String pr = m.group();
            
            prob = Double.parseDouble(pr); //probabilità della produzione grammaticale in analisi
            
            //compone la produzione grammaticale
            production = items[0];
            
            //inserimento in struttura dati apposita a seconda che sia regola di produzione lessicale o no (simbolo terminale o no)
            if(destra.length == 2){   //se a sinsitra individuo solo due elementi, la prima tra apici è un simbolo terminale
                //eliminare gli apici del terminale e comporre la stringa production
                production = production + " " + destra[0];
                lGrammar.put(production, prob);  //put production in lexical gramma hashmap
            }
            else if (destra.length > 2){//se a sinistra ci sono più gruppi di stringhe, la regola è binaria
                //System.out.println("\n"+production + " ---- ");
                for(int i=0; i < destra.length; i++){
                    production = production + " " + destra[i];
                }  
                //put production in the binary grammar hashmap
                bGrammar.put(production, prob);
            }
            else{
                throw new Exception("Input Error");
            }
            
            //keep track of non-terminal ins the grammar
            if(!nonTerminals.contains(items[0])){
                nonTerminals.add(items[0]);
            } 
       
            next = in.readLine();
        }
        in.close();
    }
 
    
     /**
     * parseSentences reads sentences from a file and sends them to the method
     * parse which parses a sentence and stores in a list of parses. Finally this method
     * prints all of those parses stored in the list
     **/
    public static void parseSentences(ArrayList<ArrayList<String>> frasi, ArrayList<String[][]> tagsProb){
        
      /*  ArrayList<String> frase = frasi.get(1);
        System.out.println(frase);  //prendo una frase dall'array list e la dò alla funzione parse, che implementa il PCKY vero e proprio
        parse(frase);  */
        for(int i = 0; i < frasi.size(); i++){
            ArrayList<String> frase = frasi.get(i);
            String[][] tags = tagsProb.get(i);
        //for (ArrayList<String> frase : frasi) {
            System.out.println(frase);  //prendo una frase dall'array list e la dò alla funzione parse, che implementa il PCKY vero e proprio
            //if(contMatch<=0)
            parse(frase, tags);
            //else break;
	}  
        
        //print parses
        for(String parse : parses){
            System.out.println(parse);
        }
    }
    
    /**
     * PCKY algorithm implementation
     * */
    public static void parse(ArrayList<String> frase, String[][] tags){
        //split words in the sentence
        
        String[] words = new String[frase.size()];
        Pattern p = Pattern.compile("\\S+");
        
        for(int i = 0; i<words.length; i++){
            words[i] = frase.get(i);
        }
        
        //initialize variables
        Double probNT1 = 0.0;
        Double probNT2 = 0.0;
        Double nextProb = 0.0;
        String rootNode = "S";
                
        Integer NT0;
        Integer NT1;
        Integer NT2;
        StringBuilder parseString = new StringBuilder();
        
        //table stores probabilties of productions from [i][j][NT]
        double[][][] table = new double[words.length+1][words.length+1][nonTerminals.size()];
        //backtrace 4-d array stores backtraces for given NTs spanning a given range in the sentence
        int[][][][] backTrace = new int[words.length+1][words.length+1][nonTerminals.size()][3];

        
        for(int j = 1; j < (words.length + 1) ; j++){
            //System.out.println(tags[j-1][0] + "    " + words[j-1] + "   " + tags[j-1][1]);
            table[j-1][j][nonTerminalToInt(tags[j-1][0])] = Double.parseDouble(tags[j-1][1]);       //la probabilità qui inserita deve essere quella data da viterbi, o la probabilità di emissione originaria?
            
           /* //PARTE TRADIZIONALE PCKY
            int flag = 0; //flag che diventa 1 non appena trovo la parola tra le produzioni nella grammatica
            
            //store all lexical production probabilties in table for word at j
            for(String prod : lGrammar.keySet()){   //per ogni produzione lessicale
               // System.out.println((prod.split("\\s+")[1]).replace(" ", "") + "      " + words[j-1].replace(" ", ""));
                
                String produzione;
                if(prod.split("\\s+")[1].contains("'"))
                    produzione = prod.split("\\s+")[1].toLowerCase().replace(" ", "").substring(1,prod.split("\\s+")[1].length()-1);
                else
                    produzione = prod.split("\\s+")[1].toLowerCase().replace(" ", "");
                //inserisci in tabella, per ciascun non terminale, la probabilità che esso porti alla parola j-esima della frase
                if(produzione.equals(words[j-1].toLowerCase().replace(" ","")))
                {
                    table[j-1][j][nonTerminalToInt(prod.split("\\s+")[0])] = lGrammar.get(prod);
                    flag = 1;
                    //break;
                } 
            }
            if(flag == 0){  //se flag è ancora uguale a 0, la parola non è stata trovata tra i terminali della grammatica!
                //smoothing: dico che la parola è un nome con probabilità 1/num.produzioni lessicali 
                table[j-1][j][nonTerminalToInt("NOUN")] = (float) 1/lGrammar.size();
                System.out.println("SMOOTHING   " + words[j-1] + "  "+ nonTerminalToInt("NOUN") + " " + table[j-1][j][nonTerminalToInt("NOUN")]);
            }
            */
            //System.out.println("********************************************************\n\n");
             
            for(int i = (j-2); i > -1; i --){
                for(int k = (i+1); k < j; k++){
                    //per ogni regola di produzione con non terminali nella grammatica A->BC
                    for(String prod : bGrammar.keySet()){ 
                        
                        NT1 = nonTerminalToInt(prod.split("\\s+")[1]); //B                         
                        NT2 = nonTerminalToInt(prod.split("\\s+")[2]); //C
                        
                        probNT1 = table[i][k][NT1]; //prob B in tabella
                        probNT2 = table[k][j][NT2];  //prob C in tabella
                        //sono sempre 0 e non enta mai nell'if!!
                                                
                        if((probNT1 > 0.0) && (probNT2 > 0.0)){   //se le prob per i due simboli non terminali nella regola analizzata è maggiore di 0
                            
                            
                            nextProb = probNT1 * probNT2 * bGrammar.get(prod);  //table[i][k]*[B]*table[k][j]*[C]*prob(A->BC)
                            NT0 = nonTerminalToInt(prod.split("\\s+")[0]);  //indice di A
                            
                          // System.out.println(prod.split("\\s+")[0]);
                            
                            if(table[i][j][NT0] < nextProb){
                                //store most probable production to this cell in table
                                //store back trace in backtrace
                              //  System.out.println(" i " + i + " j " + j + "  "+ NT0 + "     " + NT1 + "  " + NT2);
                                
                                table[i][j][NT0] = nextProb;
                                backTrace[i][j][NT0][0] = k; //k
                                backTrace[i][j][NT0][1] = NT1;  //B
                                backTrace[i][j][NT0][2] = NT2;  //C
                                
        //                        System.out.println(backTrace[i][j][NT0][0]);
        //                        System.out.println(backTrace[i][j][NT0][1]);
        //                        System.out.println(backTrace[i][j][NT0][2]);
                            }   
                        }  
                        
                    }
                }
            }
        }
        
        //return buildtree
        int[] backTraceValues = new int[3];
        int[] emptyList = {0,0,0};
        
        //get backtrace values spanning the whole sentence
        backTraceValues = backTrace[0][words.length][nonTerminalToInt(rootNode)];
        
       
        if(!Arrays.equals(backTraceValues, emptyList)){
            contMatch++;
          //System.out.println("if1");
          parseString.append("(").append(rootNode).append(" ");
          //call buildTree method
          buildTree(0,words.length,backTraceValues[0],backTraceValues[1],backTraceValues[2],parseString,backTrace,words);
          parseString.append(")");
        }
        else{
          parseString.append(" ");
        }
        //store parseString in list of parses
        parses.add(parseString.toString());
        
    }
    
    public static Integer nonTerminalToInt(String key) {
        
        return ntToInt.get(key);
    }

    /**
     * for each of the non-terminals it creates an integer representation 
     * and stores it in both optimization hashmaps
     **/
    public static void initializeNTMaps() {
        Integer key;
        for(String nt : nonTerminals){
            if(!ntToInt.containsKey(nt)){
                key = ntToInt.size();
                ntToInt.put(nt, key);
                ntToString.put(key, nt);
            }
        }
    }

    /**
     * Reconstructs the parse by looking through the backtrace table
     * */
    public static void buildTree(int i, int j, Integer k, Integer B, Integer C,StringBuilder parseString,
            int[][][][] backTrace, String[] input) {
        
          int[] backB = backTrace[i][k][B];
          int[] backC = backTrace[k][j][C];
          int[] empty = {0,0,0};
          
          //first handle B 
          parseString.append("(").append(ntToString.get(B)).append( " ");
          if( !Arrays.equals(backB,empty)){
              buildTree(i,k,backB[0],backB[1],backB[2],parseString,backTrace,input);
              parseString.append(") ");
          }
          else{
              parseString.append(input[k-1]).append(") ");
          }
          
          //handle C
          parseString.append("(").append(ntToString.get(C)).append( " ");
          if(!Arrays.equals(backC,empty)){
              buildTree(k,j,backC[0],backC[1],backC[2],parseString,backTrace,input);
              parseString.append(")");
          }
          else{
              parseString.append(input[k]).append(")");
          }
        
    }
    
}//end class
